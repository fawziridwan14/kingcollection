<?php

/**
 * This is the model class for table "mutasi".
 *
 * The followings are the available columns in table 'mutasi':
 * @property integer $id
 * @property string $tanggal
 * @property integer $toko_id
 * @property integer $produk_id
 * @property integer $qty
 * @property string $keterangan
 */
class Mutasi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mutasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tanggal, toko_id, produk_id, qty, keterangan', 'required'),
			array('toko_id, produk_id, qty', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tanggal, toko_id, produk_id, qty, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

			'toko_relasi'=>array(self::BELONGS_TO, 'Toko','toko_id'),

			'product_relasi'=>array(self::BELONGS_TO,'Product','produk_id'),			
		);
	}

	/*
	public function validasi($qty)	{
		$models = Product::model()->findAll(array('condition' => 'stock = ' . $this->product_relasi->stock, 'order'=>'id'));
		foreach($models as $model)

		if ($this->qty > $model->stock)
		 	{
				$this->addError('qty', 'Stok Product kurang!!');
			}	
		else
			return true;		
	}

	public function validasi($attribute)
	 {
		$barang = Product::model()->findByAttributes(array('product_name'=>$this->product_name));
		if($this->qty > $barang->stock)
		{
		   $this->addError('qty', 'maaf jumlah stok barang tidak mencukupi');
		   return false; 
		}
	 }*/

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tanggal' => 'Tanggal',
			'toko_id' => 'Toko',
			'produk_id' => 'Produk',
			'qty' => 'Jumlah',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('toko_id',$this->toko_id);
		$criteria->compare('produk_id',$this->produk_id);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mutasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function validasiStok($attribute)
	 {
		$product = Product::model()->findByAttributes(array('produk_id'=>$this->id));
		if($this->qty > $product->stock)
		{
		   $this->addError('qty', 'maaf jumlah stok produk tidak mencukupi');
		   return false; 
		}
	 }
}
