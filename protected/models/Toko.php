<?php

/**
 * This is the model class for table "Toko".
 *
 * The followings are the available columns in table 'Toko':
 * @property integer $toko_id
 * @property string $nama_toko
 * @property string $alamat
 * @property string $telepon
 * @property string $kota
 * @property string $tipe
 */
class Toko extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Toko';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_toko, alamat, telepon, kota, tipe', 'required'),
			array('nama_toko', 'length', 'max'=>100),
			array('alamat', 'length', 'max'=>200),
			array('telepon', 'length', 'max'=>30),
			array('kota', 'length', 'max'=>50),
			array('tipe', 'length', 'max'=>6),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('toko_id, nama_toko, alamat, telepon, kota, tipe', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'toko_id' => 'Toko',
			'nama_toko' => 'Nama Toko',
			'alamat' => 'Alamat',
			'telepon' => 'Telepon',
			'kota' => 'Kota',
			'tipe' => 'Tipe',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('toko_id',$this->toko_id);
		$criteria->compare('nama_toko',$this->nama_toko,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('telepon',$this->telepon,true);
		$criteria->compare('kota',$this->kota,true);
		$criteria->compare('tipe',$this->tipe,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Toko the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
