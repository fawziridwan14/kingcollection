<?php

class TokoController extends Controller
{ 
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin_page';

	/*const INDEX = 'index';
	
	public function actionDelete($id){
	    isAuth::Admin();
	    $model = new Toko;
	    if($model->deleteByPk($id)){
	        $this->redirect(array(self::INDEX));
	    }else{
	        throw new CHttpException(404,'Data yang di minta tidak tersedia');
	    }
	}*/
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		isAuth::Admin();
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		isAuth::Admin();
		$model=new Toko;
		$grid = Toko::model()->cache(1000)->findAll();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Toko']))
		{
			$model->attributes=$_POST['Toko'];
			if($model->save())
				$this->redirect(array('create'/*,'id'=>$model->toko_id*/));
		}

		$this->render('create',array(
			'model'=>$model,
			'grid'=>$grid,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		isAuth::Admin();
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Toko']))
		{
			$model->attributes=$_POST['Toko'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->toko_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
 	*/
	public function actionDelete($id)
	{
		isAuth::Admin();
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		isAuth::Admin();
		$dataProvider=new CActiveDataProvider('Toko');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		isAuth::Admin();
		$model=new Toko('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Toko']))
			$model->attributes=$_GET['Toko'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Toko the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Toko::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Toko $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='toko-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
