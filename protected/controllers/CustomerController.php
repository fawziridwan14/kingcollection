<?php 

class CustomerController extends Controller	{
	public $layout = 'admin_page';
	const INDEX = 'index';

	public function actionView($id)
	{
		isAuth::Admin();
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function loadModel($id)
	{
		$model=Customer::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionIndex()	{
		isAuth::Admin();
		$model = Customer::model()->cache(1000)->findAll();

		$this->render('index', array('model'=>$model));
	}

	public function actionDelete($id)	{
		isAuth::Admin();
	    $model = new Customer;
	    if($model->deleteByPk($id)){
	        $this->redirect(array(self::INDEX));
	    }else{
	        throw new CHttpException(404,'Data yang di minta tidak tersedia');
	    }
	}

	public function actionUpdate($id){
		isAuth::Admin();
	    $data=new Customer;
	    $model=$data->findByPk($id);
	     
	    if(isset($_POST['Customer'])){
	         $model->attributes=$_POST['Customer'];
	         
	        if($model->save()){
	            $this->redirect(array('index'));
	        }
	    }
	      
	    $this->render('update',array('model'=>$model));
	}	

	public function actionCreate()	{
		isAuth::Admin();
		$model=new Customer;

        if(isset($_POST['Customer']))
        {
            $model->attributes=$_POST['Customer'];
            if($model->save())
                $this->redirect(array('index'/*,'id'=>$model->id*/));
        }

        $this->render('create',array(
            'model'=>$model,
        ));		
	}	
}