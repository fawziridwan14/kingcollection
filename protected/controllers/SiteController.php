<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function actionPaypalTest() { 
	  $paypalManager = Yii::app()->getModule('SimplePaypal')->paypalManager;

	  $paypalManager->addField('item_name', 'Paypal Test Transaction');
	  $paypalManager->addField('amount', '0.01');
	  $paypalManager->addField('item_name_1', 'Test Title');
	  $paypalManager->addField('quantity_1', '2');
	  $paypalManager->addField('amount_1', '3');
	  $paypalManager->addField('custom', '111');

	  $paypalManager->dumpFields();   // for printing paypal form fields
	  //$paypalManager->submitPaypalPost();
	}	

	public function actionConfirm() {
	  if (isset($_GET['q']) && $_GET['q'] == 'success' && (isset($_POST["txn_id"]) && isset($_POST["txn_type"]))) {

	      /* ToDo: code here after user return from paypal */

	  } else {
	      throw new CHttpException(404, 'The requested page does not exist.');
	  }
	}	

	public function actionNotify() {
	  $logCat = 'paypal';
	  $paypalManager = Yii::app()->getModule('SimplePaypal')->paypalManager;
	  try {
	    if ($paypalManager->notify() && $_POST['payment_status'] === 'Completed') {
	      $model = new PaymentTransaction;
	      $model->user_id = $_POST['custom'];    // need to assign acutal user id
	      $model->mc_gross = $_POST['mc_gross'];
	      $model->payment_status = $_POST['payment_status'];
	      $model->payer_email = $_POST['payer_email'];
	      $model->verify_sign = $_POST['verify_sign'];
	      $model->txn_id = $_POST['txn_id'];
	      $model->payment_type = $_POST['payment_type'];
	      $model->receiver_email = $_POST['receiver_email'];
	      $model->txn_type = $_POST['txn_type'];
	      $model->item_name = $_POST['item_name'];
	      $model->ipn_track_id = $_POST['ipn_track_id'];
	      $model->save();

	      /* update user payement status field here */


	      Yii::log('ipn: ' . print_r($_POST, 1), CLogger::LEVEL_ERROR, $logCat);
	    } else {
	      Yii::log('invalid ipn', CLogger::LEVEL_ERROR, $logCat);
	    }
	  } catch (Exception $e) {
	    Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, $logCat);
	  }
	}

	public function actionCancel() {
		//
 	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}