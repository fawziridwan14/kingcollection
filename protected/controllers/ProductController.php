<?php

class ProductController extends YiishopController
{
	public $layout='//layouts/admin_page';

	const URLUPLOAD = '/../images/products/';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	//detail products
	public function actionReview($id)
	{ 
	 	$this->layout = 'store';
		$this->render('review', 
			array('model'=>$this->loadModel($id),
			)
		);
	}  

	public function actionIndex()
	{
		//layout store 
		$this->layout = 'store';
		// order by id descending
		$criteria = new CDbCriteria(array('order'=>'id DESC',));
		// count data produk
		$count  = Product::model()->count($criteria);
		/*panggil class pagination*/
		$pages = new CPagination($count);

		/* element per page */
		$pages->pageSize = 6;

		/* terapkan limit page */
		$pages->applyLimit($criteria);

		// select data produk cache 1000 = 10 menit
		$models = Product::model()->cache(1000)->findAll($criteria);

		/* render ke file index */
		$this->render('index', array(
			'models'=>$models,
			'pages'=>$pages,
		));
	}

	public function actionCategory($id)	
	{
		$this->layout = 'store';

			$criteria = new CDbCriteria(array('condition'=>'category_id='. $id, 'order'=>'id DESC',	));
			// count data produk
			$count  = Product::model()->count($criteria);
			/*panggil class pagination*/
			$pages = new CPagination($count);

			/* element per page */
			$pages->pageSize = 8;

			/* terapkan limit page */
			$pages->applyLimit($criteria);

			// select data produk cache 1000 = 10 menit
			$models = Product::model()->cache(1000)->findAll($criteria);

			$this->render('category', array('models'=>$models, 'pages'=>$pages,));

	}

	public function actionView($id)
	{
		IsAuth::Admin();
		$this->layout = 'admin_page';
		// $model = Product::model()->cache(1000)->findByPk($id);
		$this->render('view', 
			array('model'=>$this->loadModel($id),
			)
		);

	}
	

	public function actionDetail($id)
	{
		$this->layout = 'store';
		$model = Product::model()->cache(1000)->findByPk($id);
		// $kategori = Category::model()->cache(1000)->findAllByPk($model->category_id);
		// $merk = Brand::model()->cache(1000)->findAllByPk($model->brand_id);
		if ($model===null) {
			throw new CHttpException(404,'The requested page does not exist');
		}
		$this->render('detail',array('data'=>$model/*, 'merk'=>$merk, ,'kategori','kategori'*/));

	}	
 
 	public function actionCreate()
	{
		IsAuth::Admin();
		$model=new Product;
		if(isset($_POST['Product']))
		{
			/*cek file / gambar produk*/	
			$cekfile = $model -> image = CUploadedFile::getInstance($model, 'image');
			/*set attributes product*/
			$model -> attributes = $_POST['Product'];
			/*ambil value / nama gambar*/
			$model -> image = CUploadedFile::getInstance($model, 'image');
		 	/*jika data product disimpan*/
			if ($model -> save()) {
				/*jika file ada*/
				if (!empty($cekfile)) {
					/*set value field image dengan nama gambar
					 *dan upload gambar ke folder images/products*/
					$model -> image -> saveAs(Yii::app() -> basePath . self::URLUPLOAD . $model -> image . '');
					/*copy file yang barusan diupload ke images/products ke images/products/thumbs*/
					copy(Yii::app() -> basePath . self::URLUPLOAD . $model -> image, Yii::app() -> basePath . self::URLUPLOAD . 'thumbs/' . $model -> image);
					/*ambil filenya*/
					$name = getcwd() . '/images/products/thumbs/' . $model -> image;
					/*panggil component image dengan param $image*/
					$image = Yii::app() -> image -> load($name);
					/*resize gambar/thumb gambar*/
					$image -> resize(93, 0);
					/*simpan thumb image kembali gambar ke 
					 *images/products/thumbs*/
					$image -> save();
				}
				/*direct ke halaman product/review*/
				$this->redirect(array('view','id'=>$model->id));
			}			

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
 
	public function actionUpdate($id)
	{
		IsAuth::Admin();
		// find produk by pk
		$model=$this->loadModel($id);
		// ambil gambar
		$image = $model->image;
		// jika data perubahan dipilih
		if(isset($_POST['Product']))
		{
			// cek file image product
			$cekFile = $model->image=CUploadedFile::getInstance($model, 'image');
			if (!empty($cekFile)) {
				$model->attributes=$_POST['Product'];
				$model->image = $image;
				if($model->save())	{
					$this->redirect(array('admin','id'=>$model->id));	
				}
				
			} else {
				$model->attributes=$_POST['Product'];
				$model->image=CUploadedFile::getInstance($model, 'image');
				if ($model->save()) {
					// set value field image dengan nama gambar
					$model->image->saveAs(Yii::app()->basePath.self::URLUPLOAD.$model->image. '');
					// copy file image to thumbs folder
					copy(Yii::app()->basePath.self::URLUPLOAD.'thumbs/'. $model->image);
					// ambil file
					$name = getcwd().'/images/products/thumbs/'.$model->image;
					$image = Yii::app()->image->load($name);
					$image->resize(93,0);
					// simpan thumbs image
					$image->save();
					$this->redirect(array('view','id'=>$model->id));
				}
				
			}

		}
		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		IsAuth::Admin();
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
 
 	public function actionAdmin()
	{
		IsAuth::Admin();
		$model=new Product('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Product']))
			$model->attributes=$_GET['Product'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$model=Product::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='product-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionAddtoCart($id)	{
		$this->layout = "store";
		
		$model = new Cart;

		$_POST['Cart']['product_id'] = $id;
		$_POST['Cart']['qty'] = 1;
		$_POST['Cart']['cart_code'] = Yii::app()->session['cart_code'];
		$model->attributes = $_POST['Cart'];

		// update qty
		if ($this->addQuantity($id, Yii::app()->session['cart_code'], 1)) {
			$this->redirect(array('cart/'));
		} elseif ($model->save()) {
			$this->redirect(array('cart/'));
		} else {
			throw new CHttpException(404, 'The request id invalid.');
		}

	}

	private function addQuantity($product_id, $cart_code = '', $qty = '')	{
		$modelCart = Cart::model()->findByAttributes(array('product_id'=>$product_id, 'cart_code'=>$cart_code));

		if (count($modelCart) > 0) {
			$modelCart->qty += $qty;
			$modelCart->save();
			return TRUE;
		} else {
			return FALSE;
		}
	}

/*	public function actionSearch(){
	    $model = new Product;
	  
	    if(isset($_POST['Product'])){
	      $model->attributes = $_POST['Product'];

	      $this->redirect(array('kategory','nama'=>$model->nama));}
	    
	      $this->render('find',array('model'=>$model)); 
	}
*/
	public function actionSearch()	{
		/*gunakan layout store*/
		$this -> layout = 'store';
		if (isset($_POST['Search'])) {
			$keyword = $_POST['Search']['keyword'];
			$category = $_POST['Search']['category'];
			//$this->redirect(array('product/search','c'=>$category,'key'=>$key));

			//$criteria = new CDbCriteria( array('order' => 'id DESC', ));
			if ($category == 'all-categories' && empty($keyword)) {
				$this -> redirect(array('product/'));
			}
			if ($category != 'all-categories' && empty($keyword)) {
				$criteria = new CDbCriteria( array('order' => 'id DESC', 'condition' => 'category_id=' . $category . ''));
			}
			if ($category == 'all-categories' && !empty($keyword)) {
				$criteria = new CDbCriteria( array('order' => 'id DESC', 'condition' => 'product_name like"%' . trim($keyword) . '%"', ));
			}
			if ($category != 'all-categories' && !empty($keyword)) {
				$criteria = new CDbCriteria( array('order' => 'id DESC', 'condition' => 'category_id=' . $category . ' AND product_name like"%' . trim($keyword) . '%"', ));
			}

			/*count data product*/
			$count = Product::model() -> count($criteria);
			/*panggil class paging*/
			$pages = new CPagination($count);
			/*elements per page*/
			$pages ->pageSize = 8;
			/*terapkan limit page*/
			$pages -> applyLimit($criteria);

			/*select data product
			 *cache(1000) digunakan untuk men cache data,
			 * 1000 = 10menit*/
			$models = Product::model() -> cache(1000) -> findAll($criteria);

			/*render ke file index yang ada di views/product
			 *dengan membawa data pada $models dan
			 *data pada $pages
			 **/
			$this -> render('index', array('models' => $models, 'pages' => $pages, ));
		} else {
			$this -> redirect(array('product/'));
		}		
	}
}
