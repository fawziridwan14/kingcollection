<?php 

?>
<h3>Update #<?php echo $model->nama_supplier; ?></h3>
<hr>
<div class='form-group'>
    <?php
        echo CHtml::beginForm(array('supplier/update','id'=>$model->id));
    ?>
     
    <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'nama_supplier', array('class'=>'label-control'));
            echo CHtml::activeTelField($model, 'nama_supplier', array('class'=>'form-control'));
        ?>
    </div>
    <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'alamat', array('class'=>'label-control'));
            echo CHtml::activeTextArea($model, 'alamat', array('class'=>'form-control'));
        ?>
    </div>
    <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'telepon', array('class'=>'label-control'));
            echo CHtml::activeTelField($model, 'telepon', array('class'=>'form-control'));
        ?>
    </div>
    <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'id_admin', array('class'=>'label-control'));
            echo CHtml::activeTelField($model, 'id_admin', array('class'=>'form-control', 'value'=>Yii::app()->user->name, 'readonly'=>true));
        ?>
    </div>
    <div class='form-group pull-right'>
        <?php
            echo CHtml::submitButton('Update', array('class'=>'btn btn-primary btn-flat'));
            echo CHtml::endForm();
        ?>
    </div>
</div>