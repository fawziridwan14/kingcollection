<?php
/* @var $this BeritaController */
/* @var $dataProvider CActiveDataProvider */

/*$this->breadcrumbs=array(
	'Beritas',
);

$this->menu=array(
	array('label'=>'Create Berita', 'url'=>array('create')),
	array('label'=>'Manage Berita', 'url'=>array('admin')),
);
*/
?>
<h2 class="box-title">Semua Halaman</h2>
<div class="box">
	<div class="box-header">
			<br><br>
			<span style="left: right;padding-top: 15px;">
				<a href="<?php echo Yii::app()->createUrl('berita/create'); ?>" class="btn btn-primary btn-flat btn-sm"><i class="fa fa-plus"></i> Tambah Halaman</a><br>
			</span>
			<span style="float: left;padding-top: 15px;">
				<a href="<?php echo Yii::app()->createUrl('toko/index'); ?>" class="btn btn-default btn-flat btn-sm"> Copy</a>
				<a href="<?php echo Yii::app()->createUrl('toko/admin'); ?>" class="btn btn-default btn-flat btn-sm"> CSV</a>
				<a href="<?php echo Yii::app()->createUrl('toko/admin'); ?>" class="btn btn-default btn-flat btn-sm"> Excel</a>
				<a href="<?php echo Yii::app()->createUrl('toko/index'); ?>" class="btn btn-default btn-flat btn-sm"> PDF</a>
				<a href="<?php echo Yii::app()->createUrl('toko/admin'); ?>" class="btn btn-default btn-flat btn-sm"> Print</a>
			</span>
	</div>
		<!-- /.box-header -->
		<div class="box-body">
		  <table id="example1" class="table table-bordered table-striped">	
			<thead>
	            <tr>
	              <th>Tanggal</th>
	              <th>Judul</th>
	              <th>Status</th>
	              <th>Pembuat</th>
	              <th><center>Aksi</center></th>
	            </tr>
            </thead>
            <tbody>
				<?php foreach ($model as $var):	?>
            	<tr style="width: 100%">
            		<td><?php echo $var['tanggal']; ?></td>
            		<td><?php echo $var['judul']; ?></td>
            		<td><?php echo $var['status']; ?></td>    		            		
            		<td><?php echo $var['admin_id']; ?></td>
            		<td align="center">
            			<a href="<?php echo Yii::app()->createUrl('berita/'.$var->berita_id); ?>" class="label label-info label-flat label-md"><i class="fa fa-search fa-lg"></i> cari</a>
            			<a href="<?php echo Yii::app()->createUrl('berita/update/'.$var->berita_id); ?>" class="label label-warning label-flat label-md"><i class="fa fa-pencil fa-lg"></i> edit</a><br>
            			<a href="<?php echo Yii::app()->createUrl('berita/delete/'.$var->berita_id); ?>" class="label label-danger label-flat label-md" title="Hapus"><i class="fa fa-trash fa-lg"></i> delete</a>
            		</td>
            	</tr>
				<?php endforeach; ?>
            </tbody>		  
		  </table>
		</div>
</div>


