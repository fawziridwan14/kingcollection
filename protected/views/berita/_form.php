<?php
/* @var $this BeritaController */
/* @var $model Berita */
/* @var $form CActiveForm */
?>
<script src="<?php echo Yii::app()->baseUrl;?>/ckeditor/ckeditor.js"></script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'berita-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'berita_kategori'); ?>
		<?php 
		echo $form->dropDownList($model,'berita_kategori',CHtml::listData(KategoriBerita::model()->findAll(), 'berita_kategori_id','nama_kategori'), array(
			'empty'=>'--pilih kategori berita--',
			'class'=>'form-control',
		));
		?>
		<?php echo $form->error($model,'berita_kategori'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'judul'); ?>
		<?php echo $form->textField($model,'judul',array('size'=>60,'maxlength'=>100 ,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'judul'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'slug'); ?>
		<?php echo $form->textField($model,'slug',array('size'=>60,'maxlength'=>200,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'slug'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'isi'); ?>
		<?php 
			$this->widget('application.extensions.ckeditor.CKEditor', array(
			'model'=>$model,
			'attribute'=>'isi',
			'language'=>'en',
			'editorTemplate'=>'full',
			));
		?>
		<?php echo $form->error($model,'isi'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tanggal'); ?>
		<?php echo $form->textField($model,'tanggal', array('class'=>'form-control', 'value'=>date('Y-m-d h:i:s'), 'readOnly'=>true)); ?>
		<?php echo $form->error($model,'tanggal'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jenis'); ?>
		<?php echo ZHtml::enumDropDownList($model,'jenis',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'jenis'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo ZHtml::enumDropDownList($model,'status',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'admin_id'); ?>
		<?php echo $form->textField($model,'admin_id', array('class'=>'form-control','value'=>Yii::app()->user->name, 'readOnly'=>true)); ?>
		<?php echo $form->error($model,'admin_id'); ?>
	</div>

	<div class="form-group pull-right">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary btn-flat btn-sm')); ?>
	</div>
	<br><br>
<?php $this->endWidget(); ?>

</div><!-- form -->