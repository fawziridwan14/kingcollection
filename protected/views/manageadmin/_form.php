<?php
/* @var $this ManageadminController */
/* @var $model Admin */
/* @var $form CActiveForm */
?>

<div class="form" style="margin: 10px;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'admin-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),	 	
)); ?>

	<p class="alert alert-warning">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'email', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'username', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'password', array('class'=>'label-control')); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<?php if (!empty($model->image)) {	?>
		
	<div class="form-group">
		<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$model->image.'','image', array("style"=>"width:93px;" )); ?>
	</div>

	<?php } ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'image',array('class'=>'label-control')); ?>
		<?php echo $form->fileField($model,'image',array('size'=>50,'maxlength'=>100,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'image'); ?>
	</div>	

	<div class="form-group">
		<?php echo $form->labelEx($model,'rule', array('class'=>'label-control')); ?>
		<?php //echo $form->textField($model,'rule',array('size'=>25,'maxlength'=>25)); 
			echo $form->dropDownList($model,'rule', 
				array('admin'=>'admin','root'=>'root'),
				array('empty'=>'<--select rule-->', 'class'=>'form-control'));
		?>
		<?php echo $form->error($model,'rule'); ?>
	</div>

 	<div class="form-group">
		<?php echo $form->labelEx($model,'last_login_time', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'last_login_time', array('class'=>'form-control', 'value'=>date('Y-m-d h:i:s'),'readOnly'=>true)); ?>
		<?php echo $form->error($model,'last_login_time'); ?>
	</div> 
	<div class="form-group pull-right">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-primary btn-md')); ?>
	</div>

	<br><br><br>

<?php $this->endWidget(); ?>

</div><!-- form -->