<?php
/* @var $this ManageadminController */
/* @var $model Admin */
/* @var $form CActiveForm */
?>

<div class="wide form" style="margin: 10px;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="form-group">
		<?php echo $form->label($model,'id', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'id', array('class'=>'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'email', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'username', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'rule', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'rule',array('size'=>25,'maxlength'=>25, 'class'=>'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'image', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'image',array('size'=>25,'maxlength'=>25, 'class'=>'form-control')); ?>
	</div>


	<div class="form-group">
		<?php //echo $form->label($model,'last_login_time', array('class'=>'label-control')); ?>
		<?php //echo $form->textField($model,'last_login_time', array('class'=>'form-control')); ?>
	</div>
	<div class="form-group pull-left">
		<?php echo CHtml::submitButton('Search',array('class'=>'btn btn-primary btn-flat')); ?>
	</div>
	<br><br>

<?php $this->endWidget(); ?>

</div><!-- search-form -->