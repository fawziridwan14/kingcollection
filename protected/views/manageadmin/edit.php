<?php 

?>
<h3>Update Data User #<?php echo $model->username; ?></h3>
<hr>
<div class='form-group'>
    <?php
        echo CHtml::beginForm(array('manageadmin/edit','id'=>$model->id));
    ?>
     
    <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'username', array('class'=>'label-control'));
            echo CHtml::activeTelField($model, 'username', array('class'=>'form-control'));
        ?>
    </div>
    <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'email', array('class'=>'label-control'));
            echo CHtml::activeTextField($model, 'email', array('class'=>'form-control input-email'));
        ?>
    </div>
    <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'password', array('class'=>'label-control'));
            echo CHtml::activePasswordField($model, 'password', array('class'=>'form-control'));
        ?>
    </div>
     <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'rule', array('class'=>'label-control'));
            echo CHtml::activeTelField($model, 'rule', array('class'=>'form-control','readOnly'=>true));
        ?>
    </div>
    <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'last_login_time', array('class'=>'label-control'));
            echo CHtml::activeTelField($model, 'last_login_time', array('class'=>'form-control'));
        ?>
    </div>        

        <?php if (!empty($model->image)) {  ?>
        
    <div class="form-group">
        <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$model->image.'','image', array("style"=>"width:93px;" )); ?>
    </div>

    <?php } ?>

    <div class="form-group">
        <?php
            echo CHtml::activeLabel($model, 'image', array('class'=>'label-control'));
            echo CHtml::activeFileField($model, 'image', array('class'=>'form-control'));
        ?>
    </div>    

    <div class='form-group pull-right'>
        <?php
            echo CHtml::submitButton('Update', array('class'=>'btn btn-primary btn-flat'));
            echo CHtml::endForm();
        ?>
    </div>
    <br><br>
</div>