<?php
/* @var $this ManageadminController */
/* @var $model Admin */

$this->breadcrumbs=array(
	'Admins'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Admin', 'url'=>array('index')),
	array('label'=>'Create Admin', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#admin-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
}); 
");
?> 
<br>
<div class="box box-primary">
    <div class="box-header with-border">
	    <h3 class="box-title">Kelola Data User</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('manageadmin/index'); ?>" class="btn btn-info"><i class="fa fa-list"></i> List User</a>
			<a href="<?php echo Yii::app()->createUrl('manageadmin/create'); ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah User</a>
		</span>
    </div>

<div style="padding: 10px;">
	<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
</div>

<div class="search-form" style="display:none;">
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
</div><!-- search-form -->

<style type="text/css">
	.tabel {
		padding: 10px;
	}
</style>

	<div class="table table-responsive table-bordered tabel">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'admin-grid',
				'dataProvider'=>$model->search(),
				'filter'=>$model,
				'columns'=>array(
					'id',
					'email',
					'username',
					'password',
					'last_login_time',
					array('name'=>'image', 
		              'type' => 'html',
		              'value' => 'CHtml::image(Yii::app()->baseUrl . "/images/" . $data->image,"", array("width"=>50, "height"=>50))',
		           	),
					array(
						'header'=>'Aksi',
						'class'=>'CButtonColumn',
						'template'=>'{update}{view}{delete}'
					),
				),
			)); ?>		
	</div>

</div>