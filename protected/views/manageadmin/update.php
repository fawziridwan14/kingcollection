<?php
/* @var $this ManageadminController */
/* @var $model Admin */

$this->breadcrumbs=array(
	'Admins'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Admin', 'url'=>array('index')),
	array('label'=>'Create Admin', 'url'=>array('create')),
	array('label'=>'View Admin', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Admin', 'url'=>array('admin')),
);
?>
<br>
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Ubah User #<?php echo $model->id; ?></h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('manageadmin/index'); ?>" class="btn btn-primary" title="List User Admin"><i class="fa fa-bars"></i></a>
			<!-- echo Yii::app()->createUrl('manageadmin/delete/'.$model->id); -->
			<a href="<?php echo Yii::app()->createUrl('manageadmin/'.$model->id); ?>" class="btn btn-info" title="View User Admin"><i class="fa fa-search"></i></a>
			<a href="<?php echo Yii::app()->createUrl('manageadmin/create'); ?>" class="btn btn-success" title="Tambah User Admin"><i class="fa fa-plus"></i></a>
			<a href="<?php echo Yii::app()->createUrl('manageadmin/admin'); ?>" class="btn btn-danger" title="Kelola manageadmin Transfer"><i class="fa fa-cogs"></i></a>
		</span>
	</div>
	<div style="margin: 10px;">
		<?php $this->renderPartial('_form', array('model'=>$model)); ?>		
	</div> 
</div>