<?php
/* @var $this MutasiController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mutasis',
);

$this->menu=array(
	array('label'=>'Create Mutasi', 'url'=>array('create')),
	array('label'=>'Manage Mutasi', 'url'=>array('admin')),
);
?>

<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">List Mutasi Stok</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('mutasi/create'); ?>" class="btn btn-success btn-flat"><i class="fa fa-plus"></i> Tambah Mutasi Stok</a>
			<a href="<?php echo Yii::app()->createUrl('mutasi/admin'); ?>" class="btn btn-danger btn-flat"><i class="fa fa-cogs"></i> Kelola Data</a>
		</span>
	</div>
	
	<div style="margin: 10px;">
		<?php $this->widget('zii.widgets.CListView', array(
			'dataProvider'=>$dataProvider,
			'itemView'=>'_view',
		)); ?>
		<br>
	</div>

</div>