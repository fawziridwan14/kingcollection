<?php
/* @var $this MutasiController */
/* @var $model Mutasi */
/* @var $form CActiveForm */
?>

<div class="form" style="margin: 10px;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mutasi-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tanggal'); ?>
		<?php echo $form->textField($model,'tanggal', array('class'=>'form-control date', 'value'=>date('Y-m-d'), 'readOnly'=>true)); ?>
		<?php echo $form->error($model,'tanggal'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'toko_id'); ?>
		<?php echo $form->dropDownList($model,'toko_id',CHtml::listData(Toko::model()->findAll(), 'toko_id','nama_toko'), array('empty'=>'Toko','class'=>'form-control select2',
			));
		?>
		<?php echo $form->error($model,'toko_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'produk_id'); ?>
		<?php echo $form->dropDownList($model,'produk_id',CHtml::listData(Product::model()->findAll(), 'id','product_name','stock'), array('empty'=>'Produk','class'=>'form-control select2',
			));
		?>
		<?php echo $form->error($model,'produk_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'qty'); ?>
		<?php echo $form->numberField($model,'qty', array('class'=>'form-control','id'=>'qty','value'=>'0')); ?>
		<?php echo $form->error($model,'qty'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'keterangan'); ?>
		<?php echo $form->textArea($model,'keterangan',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'keterangan'); ?>
	</div>

	<div class="form-group pull-right">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-flat btn-primary btn-sm')); ?>
	</div>
	<br><br><br>

<?php $this->endWidget(); ?>

</div><!-- form -->