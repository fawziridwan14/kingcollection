<?php
/* @var $this MutasiController */
/* @var $model Mutasi */

$this->breadcrumbs=array(
	'Mutasis'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Mutasi', 'url'=>array('index')),
	array('label'=>'Create Mutasi', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#mutasi-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Transaksi Mutasi Stok</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('mutasi/index'); ?>" class="btn btn-info btn-flat"><i class="fa fa-bars"></i> Lihat Mutasi Stok</a>
			<a href="<?php echo Yii::app()->createUrl('mutasi/create'); ?>" class="btn btn-success btn-flat"><i class="fa fa-plus"></i> Tambah Mutasi Stok</a>
		</span>
	</div>
	

	<div style="margin: 10px;">
	<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
	<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
	</div><!-- search-form -->
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'mutasi-grid',
		'dataProvider'=>$model->search(),
		'pagerCssClass'=>'pagination',
	    'ajaxUpdate'=>true,
		'filter'=>$model,
		'columns'=>array(
			'id',
			'tanggal',
			'toko_id',
			'produk_id',
			'qty',
			'keterangan',
			array(
				'class'=>'CButtonColumn',
			),
		),
	)); ?>		
	</div>

</div>