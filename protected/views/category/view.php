<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Category', 'url'=>array('index')),
	array('label'=>'Create Category', 'url'=>array('create')),
	array('label'=>'Update Category', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Category', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Category', 'url'=>array('admin')),
);
?> 
<br>
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">View Category #<?php echo $model->id; ?></h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('category/create'); ?>" class="btn btn-info" title="List Kategori"><i class="fa fa-bars"></i></a>
			<a href="<?php echo Yii::app()->createUrl('category/create'); ?>" class="btn btn-primary" title="Tambah Kategori"><i class="fa fa-plus"></i></a>
			<a href="<?php echo Yii::app()->createUrl('category/update/'.$model->id); ?>" class="btn btn-warning" title="Update Kategori"><i class="fa fa-pencil"></i></a>
			<a href="<?php echo Yii::app()->createUrl('category/delete/'.$model->id); ?>" class="btn btn-danger" title="Delete Bank Transfer"><i class="fa fa-trash"></i></a>
			<a href="<?php echo Yii::app()->createUrl('category/admin'); ?>" class="btn btn-success" title="Kelola Kategori"><i class="fa fa-cogs"></i></a>
		</span>
	</div>
	<div style="margin: 10px;">
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			'id',
			'category_name',
		),
	)); ?>
	</div>
	<br>
</div>

