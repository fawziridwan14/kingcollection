<?php
/* @var $this BankController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Banks',
);

$this->menu=array(
	array('label'=>'Create Bank', 'url'=>array('create')),
	array('label'=>'Manage Bank', 'url'=>array('admin')),
);
?>

<br><br>

<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Bank Transfer</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('bank/create'); ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Bank Transfer</a>
			<a href="<?php echo Yii::app()->createUrl('bank/admin'); ?>" class="btn btn-danger"><i class="fa fa-cogs"></i> Kelola Bank Transfer</a>
		</span>
	</div>
	
	<div style="margin: 10px;">
		<?php $this->widget('zii.widgets.CListView', array(
			'dataProvider'=>$dataProvider,
			'itemView'=>'_view',
		)); ?>		
	</div>
	<br>
</div>

  