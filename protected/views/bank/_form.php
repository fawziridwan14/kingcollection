<?php
/* @var $this BankController */
/* @var $model Bank */
/* @var $form CActiveForm */
?>

<div class="form" style="margin: 10px;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bank-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="alert alert-warning">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_bank', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'nama_bank',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'nama_bank'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'pemilik', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'pemilik',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'pemilik'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'no_rek', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'no_rek',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'no_rek'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'logo', array('class'=>'label-control')); ?>
		<?php echo $form->fileField($model,'logo',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'logo'); ?>
	</div>
	<div class="form-group pull-right">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-primary btn-flat')); ?>
	</div>
	<br><br><br><br>

<?php $this->endWidget(); ?>

</div><!-- form -->