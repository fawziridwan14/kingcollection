<?php
/* @var $this BankController */
/* @var $model Bank */

$this->breadcrumbs=array(
	'Banks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Bank', 'url'=>array('index')),
	array('label'=>'Manage Bank', 'url'=>array('admin')),
);
?>
<br>
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Tambah Bank Transfer</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('bank/admin'); ?>" class="btn btn-danger"><i class="fa fa-cogs"></i> Kelola Bank Transfer</a>
			<a href="<?php echo Yii::app()->createUrl('bank/index'); ?>" class="btn btn-primary"><i class="fa fa-bars"></i> List Bank Transfer</a>
		</span>
	</div>
	<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>