<?php
/* @var $this BrandController */
/* @var $model Brand */
/* @var $form CActiveForm */
?>

<div class="form" style="margin: 10px;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'brand-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="alert alert-warning">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'brand_name', array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'brand_name',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'brand_name'); ?>
	</div>

	<div class="form-group pull-right">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-flat btn-primary')); ?>
	</div>
	<br><br><br><br>
<?php $this->endWidget(); ?>

</div><!-- form -->