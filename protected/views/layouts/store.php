<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo Yii::app()->name; ?></title>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" rel="stylesheet">    
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" rel="stylesheet">    
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/css/jquery.bxslider.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/css/style.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/style.css" rel="stylesheet">     
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/plugins/datatables/dataTables.bootstrap.css">
    
    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/custom.js"></script>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/plugin/prettyphoto/css/prettyPhoto.css">
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/plugin/prettyphoto/js/jquery.prettyPhoto.js"></script>    <script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
	    $("a[rel^='prettyPhoto']").prettyPhoto({
	    	default_width: 500,
			default_height: 344, 
			sosial_tools:"",
	    });
	});
	</script>

    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>

</head>
<body>
<!-- Start of LiveChat (www.livechatinc.com) code -->
<!-- <script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 8997810;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
 --><!-- End of LiveChat code -->
	<header class="hidden-print">
	    <div class="container">
	        <div class="row">

	        	<!-- Logo -->
	            <div class="col-lg-10 col-md-3 hidden-sm hidden-xs">
	            	<div class="well logo">
	            		<a href="<?php echo Yii::app()->request->baseUrl;?>/product/index">
	            			<span><?php echo Yii::app()->name; ?> </span>
	            		</a>	            		
	            	</div>
	            </div>
	            <!-- End Logo -->
 
	            <!-- Shopping Cart List -->
	            <div class="col-lg-2 col-md-4 col-sm-5"> 
                    <div class="well">
	                    <div class="btn-group btn-group-cart">
                        <?php if (!Yii::app()->user->isGuest): ?>
                            <div class="btn-group">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                Akun Saya <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li><a href="<?php echo Yii::app()->request->baseUrl;?>/account/info">Profil</a></li>
                                <li><a href="<?php echo Yii::app()->request->baseUrl;?>/account/orders">Histori Belanja</a></li>
                                <li role="separator" class="divider"></li>
                                <li>
                                <?php if(!Yii::app()->user->isGuest): ?>                                
                                <!-- <a href="<?php //echo Yii::app()->request->baseUrl;?>/account/logout">Logout <?php //echo '('.Yii::app()->user->customerLogin.')'; ?></a>  -->
                                <?php endif; ?>     
                                <?php if  (Yii::app()->user->customerLogin) {
                                    echo CHtml::link('Logout',array('account/logout')).' ';
                                } ?>                               
                                </li>
                              </ul>
                            </div>
                        <?php endif; ?>    
                            <?php if(Yii::app()->user->isGuest): ?>
                            <a href="<?php echo Yii::app()->request->baseUrl;?>/account/index">Login</a> 
                            <?php endif; ?> 
	                    </div>
	                </div>
	            </div>
	            <!-- End Shopping Cart List -->
	        </div>
	    </div>
    </header>

	<!-- Navigation -->
    <nav class="navbar navbar-inverse hidden-print" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- text logo on mobile view -->
                <a class="navbar-brand visible-xs" href="<?php echo Yii::app()->request->baseUrl;?>/product/index">King Collection Online Shop</a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">                	
                    <li><a href="<?php echo Yii::app()->request->baseUrl;?>/product/index">Home</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl;?>/product/" class="">Produk</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl;?>/cart" class="">Keranjang</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl;?>/account/orders" class="">Tracking Order</a></li>
                    <li class="nav-dropdown">
                    	<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							Informasi <span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
    						<li><a href="<?php echo Yii::app()->createUrl('site/page&view=about'); ?>">Tentang Kami</a></li>
    						<li><a href="http://localhost/neraca/informasi/kontak-kami"> Kontak Kami</a></li>
    						<li><a href="http://localhost/neraca/informasi/cara-pembelian">Cara Pembelian</a></li>
    						<li><a href="http://localhost/neraca/informasi/konfirmasi-pembayaran">Konfirmasi Pembayaran</a></li>                           
                        </ul>
                    </li>
                </ul>				
            </div>
        </div>
    </nav>
    <!-- End Navigation -->

    <div class="container main-container">

        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 hidden-print">
<!-- Categories -->
<div class="col-lg-12 col-md-12 col-sm-6">

    <div class="no-padding">
        <span class="title">Cari Produk</span>
    </div>

    <div id="main_menu">
        <div class="list-group panel panel-search">
            <form action="<?php echo $this->createUrl('product/search');?>" method="post">
                <div class="input-group">
                    <select class="form-control" name="Search[category]">
                        <option value="all-categories">Pilih Kategori</option>
                        <?php
                        $Categories = Category::model()->findAll();
                        foreach($Categories as $category):
                        ?> 
                        <option value="<?php echo $category->id;?>"><?php echo $category->category_name;?></option>
                        <?php endforeach;?>  
                        &nbsp;                  
                    </select> 
                    &nbsp;
                    <input name="Search[keyword]" class="form-control" type="text" placeholder="Cari produk apapun"" class="text" />   
                    <br><br><br><br>
                    <div align="right">
                        <span class="input-group-btn"> 
                            <button class="btn btn-primary no-border-left" type="submit"><i class="fa fa-search"></i> Cari</button>
                        </span>                        
                    </div> 
                </div> 
            </form>
        </div>
    </div>

</div>
<!-- End Categories -->

<!-- Categories -->
<div class="col-lg-12 col-md-12 col-sm-6">

    <div class="no-padding">
        <span class="title">Kategori</span>
    </div>

        <div id="main_menu">
            <div class="list-group panel panel-cat">
            <?php 
                $model =  Category::model()->findAll();
                foreach($model as $data):
            ?>        
            <a href="<?php echo Yii::app()->request->baseUrl;?>/product/category/<?php echo $data["id"]; ?>" class="list-group-item" ><?php echo $data["category_name"]; ?></a>                             
            <?php endforeach; ?>

            </div>
        </div>

    </div>
    <!-- End Categories -->


	<!-- Best Seller -->
	<div class="col-lg-12 col-md-12 col-sm-6">
		<div class="no-padding">
    		<span class="title">Produk Lainnya</span>
    	</div>
    		<?php 
                $model =  Product::model()->findAll();
                foreach($model as $data):
            ?>
            <div class="hero-feature hidden-sm">
                <div class="thumbnail text-center">
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/product/detail/<?php echo $data["id"]; ?>" class="link-p">
                        <img src="<?php echo Yii::app()->request->baseUrl.'/images/products/thumbs/'. $data->image; ?>" alt="">
                    </a>
                    <div class="caption prod-caption">
                        <h4><a href="<?php echo Yii::app()->request->baseUrl; ?>/product/detail/<?php echo $data["id"]; ?>"><?php echo $data["product_name"]; ?></a></h4>
                        <p>
                            <div class="btn-group">
                                <a href="javascript:;" class="btn btn-default btn-sm">Rp <?php echo $data["varPrice"]; ?></a>
                                <?php 
                                    echo CHtml::link('<i class="fa fa-shopping-cart"></i> Beli',array('addtocart', 'id'=>$data->id,'p'=>$data->product_name),array(
                                        'class'=>'btn btn-app btn-primary btn-sm',
                                    ));
                                ?>
                                <!-- <a href="http://localhost/neraca/produk/add/8" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Beli</a> -->
                            </div>
                        </p>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
<div style="float: right; margin-top: 5px; margin-bottom: 7px; margin-right: 18px;clear: both;">
    <?php 
    /*$this->widget('CLinkPager', array(
       'pages' => $pages,
       //'cssFile'=>Yii::app()->request->baseUrl.'/css/pager2.css',
       'header'=> '',
       'firstPageLabel'=>'| <',
       'lastPageLabel'=>'> |',
       'nextPageLabel'=>'>',
       'prevPageLabel'=>'<',
       'maxButtonCount'=>5,
    ));*/
    ?> 
</div>            
</div>
<!-- End Best Seller -->

</div>

        	<div class="clearfix visible-sm"></div>

            <div style="text-align: justify; float: left;">

                <?php if(isset($this->breadcrumbs)):?>
                    <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                        'links'=>$this->breadcrumbs,
                    )); ?><!-- breadcrumbs -->
                <?php endif?>

            </div>
          
            <!-- Featured -->
            <?php echo $content; ?>

        	<div class="clearfix visible-sm"></div>
        </div>
	</div>

	<footer class="hidden-print">
    	<div class="container">
        	<div class="col-lg-3 col-md-3 col-sm-6">
        		<div class="column">
        			<h4>Information</h4>
        			<ul>
    					<li><a href="http://localhost/neraca/informasi/tentang-kami">Tentang Kami</a></li>
						<li><a href="http://localhost/neraca/informasi/kontak-kami">Kontak Kami</a></li>
						<li><a href="http://localhost/neraca/informasi/cara-pembelian">Cara Pembelian</a></li>
						<li><a href="http://localhost/neraca/informasi/konfirmasi-pembayaran">Konfirmasi Pembayaran</a></li>
        			</ul>
        		</div>
        	</div>

            <div class="col-lg-3 col-md-3 col-sm-6">
        		<div class="column">
        			<h4>Kategori</h4>
        			<ul>
                    <?php 
                        $model =  Category::model()->findAll();
                        foreach($model as $data):
                    ?>        
					<li><a href="<?php echo Yii::app()->request->baseUrl;?>/product/category/<?php echo $data["id"]; ?>"><?php echo $data["category_name"]; ?></a></li>
                    <?php endforeach; ?>
        			</ul>
        		</div>
        	</div>
        	<div class="col-lg-3 col-md-3 col-sm-6">
        		<div class="column">
        			<h4>Kontak Info</h4>
        			<ul>
        				<li><a href="http://localhost/neraca/kontak-kami">Kontak Kami</a></li>
        				<li><a href="#">Neraca Shoes</a></li>
        				<li><a href="#"><i class="fa fa-phone"></i> 0751-21443</a></li>
        				<li><a href="#"><i class="fa fa-envelope"></i> neraca@gmail.com</a></li>
        			</ul>
        		</div>
        	</div>
        	<div class="col-lg-3 col-md-3 col-sm-6">
        		<div class="column">
        			<h4> Follow Us</h4>
        			<ul class="social">
        				<li><a href="#" class="fa fa-google-plus"> Google Plus</a></li>
        				<li><a href="#" class="fa fa-facebook"> Facebook</a></li>
        				<li><a href="#" class="fa fa-twitter"> Twitter</a></li>
        				<li><a href="#" class="fa fa-feed"> RSS Feed</a></li>
        			</ul>
        		</div>
        	</div>
        </div>
        <div class="navbar-inverse copyright">
        	<p class="pull-left">Copyright &copy; 2017 King Collection</p>
            <p class="pull-right">Author By <b>Fawzi Ridwan</b></p>
            <br>
        </div>
    </footer>

    <a href="#top" class="back-top text-center hidden-print" onclick="$('body,html').animate({scrollTop:0},500); return false">
    	<i class="fa fa-angle-double-up"></i>
    </a>

    
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/js/bootstrap.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/js/jquery.bxslider.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/js/jquery.blImageCenter.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/js/mimity.js"></script>
</body>
</html>