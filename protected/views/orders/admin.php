<?php
/* @var $this ManageadminController */
/* @var $model Admin */

$this->breadcrumbs=array(
	'Order'=>array('index'),
	'Manage Orders',
);

?>
<br>
<div class="box box-primary">
    <div class="box-header with-border">
	    <h3 class="box-title">Manage Orders</h3>
    </div>

	<div style="margin: 10px;"> 
		<?php 
		/*gunakan data grid view*/
		$this->widget('zii.widgets.grid.CGridView', array(
			/*id datagridview*/
			'id'=>'order-grid',
			/*data provider (data order)*/
			'dataProvider'=>$model->search(),
			/*untuk filter/pencarian*/
			'filter'=>$model,
			/*untuk menghilangkan 
			 *summary text paging*/
			'summaryText'=>'',
			'pager'=>array(
					'header'=>'',
			),
			/*data yang dampilkan*/
			'columns'=>array(
				/*id order*/
				'id',
				/*order kode/nomor pemesanan*/
				'order_code',
				/*tanggal pemesanan*/
				'order_date',
				/*bank transfer*/
				'bank_transfer',
				/*payment status*/
				array(
					'name' => "payment_status",
					'value'=>'$data->payment',
					'type'=>'html',
					/*untuk filter pembayaran (sudah bayar/belum)*/
					'filter' => CHtml::dropDownList('Order[payment_status]',$model->payment_status,array('1'=>'Paid','0'=>'Pending'),array('empty'=>'')),
				),
				array(
					'name'=>'ongkir',
					'value'=>'Yii::app()->numberFormatter->format("IDR ###,###,###",$data->ongkir)',
				),
				array(
					'type'=>'HTML',
					'name'=>'grandtotal',
					'value'=>'Yii::app()->numberFormatter->format("IDR ###,###,###",$data->grandtotal)',
				),
				array(
					'header'=>'Aksi',
					'class'=>'CButtonColumn',
					/*template buttom aksi:
					 *menampilkan tombol view saja*/
					'template'=>'{view}'
				),
			),
		)); ?>
	</div>

</div>