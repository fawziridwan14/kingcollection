<?php
/* @var $this ProductController */
/* @var $model Product */
/* @var $form CActiveForm */
?>

<div class="form" style="margin: 10px;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),	
)); ?>

	<p class="alert alert-warning">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'product_name',array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'product_name',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'product_name'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'category_id',array('class'=>'label-control')); ?>
		<?php 
		//echo $form->textField($model,'category_id'); 
		echo $form->dropDownList($model,'category_id',CHtml::listData(Category::model()->findAll(), 'id','category_name'), array(
			'empty'=>'--pilih kategori--',
			'class'=>'form-control',
		));
		?>
		<?php echo $form->error($model,'category_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'brand_id',array('class'=>'label-control')); ?>
		<?php //echo $form->textField($model,'brand_id'); 
				echo $form->dropDownList($model,'brand_id',CHtml::listData(Brand::model()->findAll(), 'id','brand_name'), array(
			'empty'=>'--pilih merek--',
			'class'=>'form-control',
		));
		?>
		<?php echo $form->error($model,'brand_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'price',array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'price',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'price'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'description',array('class'=>'label-control')); ?>
		<?php echo $form->textArea($model,'description',array('form-groups'=>6, 'cols'=>50,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<?php if (!empty($model->image)) {	?>
		
	<div class="form-group">
		<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/products/'.$model->image.'','image', array("style"=>"width:93px;" , 'class'=>'form-control')); ?>
	</div>

	<?php } ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'image',array('class'=>'label-control')); ?>
		<?php echo $form->FileField($model,'image',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'image'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'berat',array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'berat',array('class'=>'form-control','value'=>'0')); ?>
		<?php echo $form->error($model,'berat'); ?>
	</div>		

	<div class="form-group">
		<?php echo $form->labelEx($model,'stock',array('class'=>'label-control')); ?>
		<?php echo $form->numberField($model,'stock',array('class'=>'form-control','value'=>'0')); ?>
		<?php echo $form->error($model,'stock'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jumlah_masuk',array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'jumlah_masuk',array('class'=>'form-control','value'=>'0')); ?>
		<?php echo $form->error($model,'jumlah_masuk'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jumlah_keluar',array('class'=>'label-control')); ?>
		<?php echo $form->textField($model,'jumlah_keluar',array('class'=>'form-control','value'=>'0')); ?>
		<?php echo $form->error($model,'jumlah_keluar'); ?>
	</div>		

	<div class="form-group pull-right">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-primary btn-flat')); ?>
	</div>

<?php $this->endWidget(); ?>
	<br><br><br>
</div><!-- form -->