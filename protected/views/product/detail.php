<div class="col-lg-9 col-md-9 col-sm-12">
	<div class="col-lg-12 col-sm-12 hidden-print">

		<span class="title">Beli Produk <?php echo $data["product_name"]; ?></span>
	</div>
            	
        	<div class="col-lg-4 col-sm-4">
				<div class="thumbnail">
					<a href="#" rel="prettyPhoto"><img style="width: 300px; height: 300px;" src="<?php echo Yii::app()->request->baseUrl.'/images/products/thumbs/'. $data->image; ?>" alt=""></a><br/>
					<h3><center>Rp <?php echo $data["varPrice"]; ?></center></h3>
				</div>
			</div>
	
	<div class="col-lg-8 col-sm-8">
		<h4><?php echo $data['product_name']; ?></h4>
		<p>
			<br>
			Kategori : <a href="<?php echo Yii::app()->request->baseUrl;?>/product/category/<?php echo $data["category_id"]; ?>" target="_blank"><?php echo $data->category->category_name; ?></a>
			<br>

			Merek : <a href="#" target="_blank"><?php echo $data->brand->brand_name ?></a> 
			<br/>
			Stok : 
			<?php 
				if ($data["stock"] > 0) {
					echo '<b style="color: green;">Tersedia</b>';
				} else {
					echo '<b style="color: red;">Tidak Tersedia</b>';
				}
			?>
		</p>
			<p><p>Deskripsi <b><?php echo $data["product_name"]; ?></b></p>
			   <p class="panel panel-body panel-success"><?php echo $data["description"]; ?></p><br>
		</p>	
		<p>
			<div class="form-group">
				<label class="col-sm-2 control-label">&nbsp;</label>
				<div class="col-md-6">
					<?php //if (!Yii::app()->user->isGuest) { echo CHtml::link('<i class="fa fa-pencil"></i> Update',array('update', 'id'=>$data->id,'p'=>$data->product_name),array('class'=>'btn btn-app btn-warning btn-sm',));} ?>
					&nbsp;&nbsp;  
					<?php if ($data["stock"] > 0) {
						echo CHtml::link('<i class="fa fa-shopping-cart"></i> Beli',array('addtocart', 'id'=>$data->id),array(
						    'class'=>'btn btn-app btn-primary btn-sm',
						));
					} else {
						'<b style="color: red;">Tidak Tersedia</b>';
					}
					?>					
				</div>
			</div>
			</form>	
		</p>
	</div>
</div>
<!-- End Featured -->