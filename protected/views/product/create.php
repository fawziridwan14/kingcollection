<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Products'=>array('index'),
	'Create',
);

/*$this->menu=array(
	array('label'=>'List Product', 'url'=>array('index')),
	array('label'=>'Manage Product', 'url'=>array('admin')),
);*/
?>
<br>
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Tambah Data Produk</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('product/admin'); ?>" class="btn btn-danger"><i class="fa fa-cogs"></i> Kelola Data</a>
		</span>
	</div>
	<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>