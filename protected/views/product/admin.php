<?php

$this->menu=array(
	array('label'=>'List Product', 'url'=>array('index')),
	array('label'=>'Create Product', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#product-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<br>
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Kelola Data Produk</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('product/create'); ?>" class="btn btn-primary btn-flat btn-sm"><i class="fa fa-plus"></i> Tambah Produk</a>
			<a href="<?php echo Yii::app()->createUrl('product/create'); ?>" class="btn btn-info btn-flat btn-sm"><i class="fa fa-bars"></i> List Produk</a>
		</span>
	</div>
	<div style="margin: 10px;">
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'product-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
	    'pagerCssClass'=>'pagination',
	    'ajaxUpdate'=>true, 
		'summaryText'=>'',
		'pager'=>array(
			'header'=>'',
			'firstPageLabel'=>'| <',
			'lastPageLabel'=>'> |',
			'nextPageLabel'=>'>',
			'prevPageLabel'=>'<',
		),
		'columns'=>array(
			'product_name',
			array('name'=>'category_id',
				'type'=>'html',
				'value'=>'$data->category->category_name',
				'sortable'=>TRUE,
				'filter'=>CHtml::listData(Category::model()->findAll(),'id','category_name'),
			),
			array('name'=>'brand_id',
				'type'=>'html',
				'value'=>'$data->brand->brand_name',
				'sortable'=>TRUE,
				'filter'=>CHtml::listData(Brand::model()->findAll(),'id','brand_name'),
			),
			array('name'=>'price',
				'type'=>'html',
				'value'=>'$data->varPrice',
				'sortable'=>TRUE,
			),		
			// 'description',
			array('name'=>'image', 
	          'type' => 'html',
	          'value' => 'CHtml::image(Yii::app()->baseUrl . "/images/products/thumbs/" . $data->image,"", array("width"=>50, "height"=>50))',
	       	),
			'stock',
			'berat',
			array(
				'header'=>'Aksi',
				'class'=>'CButtonColumn',
				'template'=>'{update}{view}{delete}'
			),
		), 
	)); ?>		
	</div>    	
</div>