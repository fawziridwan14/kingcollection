<?php
/* @var $this TokoController */
/* @var $model Toko */

$this->breadcrumbs=array(
	'Tokos'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Toko', 'url'=>array('index')),
	array('label'=>'Create Toko', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#toko-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?> 

<br><br>
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Manage Toko</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('toko/index'); ?>" class="btn btn-primary"><i class="fa fa-bars"></i> List Toko</a>
			<a href="<?php echo Yii::app()->createUrl('toko/create'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Toko</a>
		</span>
	</div>
	<div style="margin: 10px;">
		<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
		<div class="search-form" style="display:none">
		<?php $this->renderPartial('_search',array(
			'model'=>$model,
		)); ?>		
	</div>
	</div><!-- search-form -->	
	<div style="margin: 10px;">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'toko-grid',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns'=>array(
				'toko_id',
				'nama_toko',
				'alamat',
				'telepon',
				'kota',
				'tipe',
				array(
					'class'=>'CButtonColumn',
				),
			),
		)); ?>
	</div>	
</div>