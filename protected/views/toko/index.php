<?php
/* @var $this TokoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tokos',
);

$this->menu=array(
	array('label'=>'Create Toko', 'url'=>array('create')),
	array('label'=>'Manage Toko', 'url'=>array('admin')),
);
?>

<br><br>

<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Toko Outlet/Cabang</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('toko/create'); ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Toko</a>
			<a href="<?php echo Yii::app()->createUrl('toko/admin'); ?>" class="btn btn-danger"><i class="fa fa-cogs"></i> Kelola Toko</a>
		</span>
	</div>
	
	<div style="margin: 10px;">
		<?php $this->widget('zii.widgets.CListView', array(
			'dataProvider'=>$dataProvider,
			'itemView'=>'_view',
		)); ?>		
	</div>
	<br>
</div>
