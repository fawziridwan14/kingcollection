<?php
/* @var $this TokoController */
/* @var $model Toko */
$this->breadcrumbs=array(
	'Toko'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Toko', 'url'=>array('index')),
	array('label'=>'Manage Toko', 'url'=>array('admin')),
);
?>
<br><br>

<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">Tambah Toko</h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('toko/index'); ?>" class="btn btn-primary btn-flat"><i class="fa fa-list"></i> List Toko</a>
			<a href="<?php echo Yii::app()->createUrl('toko/admin'); ?>" class="btn btn-success btn-flat"><i class="fa fa-cogs"></i> Kelola Toko</a>
		</span>
	</div>
	
	<?php $this->renderPartial('_form', array('model'=>$model)); ?> 

</div>	

<div class="box">
	<div class="box-header">
	  <h3 class="box-title">Data Toko Outlet/Cabang</h3>
	</div>
		<!-- /.box-header -->
		<div class="box-body">
		  <table id="example1" class="table table-bordered table-striped">	
			<thead>
	            <tr>
	              <th><?php echo CHtml::activeLabel($model, 'nama_toko'); ?></th>
	              <th><?php echo CHtml::activeLabel($model, 'alamat'); ?></th>
	              <th><?php echo CHtml::activeLabel($model, 'telepon'); ?></th>
	              <th><?php echo CHtml::activeLabel($model, 'kota'); ?></th>
	              <th><?php echo CHtml::activeLabel($model, 'tipe'); ?></th>
	              <th>Aksi</th>
	            </tr>
            </thead>
            <tbody>
				<?php foreach ($grid as $model):	?>
            	<tr style="width: 100%">
            		<td><?php echo $model['nama_toko']; ?></td>
            		<td><?php echo $model['alamat']; ?></td>
            		<td><?php echo $model['telepon']; ?></td>
            		<td><?php echo $model['kota']; ?></td>
            		<td><?php echo $model['tipe']; ?></td>
            		<td align="center">
            			<a href="<?php echo Yii::app()->createUrl('toko/'.$model->toko_id); ?>" class="btn btn-info btn-flat btn-sm"><i class="fa fa-search fa-lg"></i></a>
            			<a href="<?php echo Yii::app()->createUrl('toko/update/'.$model->toko_id); ?>" class="btn btn-warning btn-flat btn-sm"><i class="fa fa-pencil fa-lg"></i></a>
            			<a href="<?php echo Yii::app()->createUrl('toko/delete/'.$model->toko_id); ?>" class="btn btn-danger btn-flat btn-sm" title="Hapus"><i class="fa fa-trash fa-sm"></i></a>
            		</td>
            	</tr>
				<?php endforeach; ?>
            </tbody>		  
		  </table>
		</div>
</div>