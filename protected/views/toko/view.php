<?php
/* @var $this TokoController */
/* @var $model Toko */

$this->breadcrumbs=array(
	'Tokos'=>array('index'),
	$model->toko_id,
);

$this->menu=array(
	array('label'=>'List Toko', 'url'=>array('index')),
	array('label'=>'Create Toko', 'url'=>array('create')),
	array('label'=>'Update Toko', 'url'=>array('update', 'id'=>$model->toko_id)),
	array('label'=>'Delete Toko', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->toko_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Toko', 'url'=>array('admin')),
);
?>

<br>  
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">View Data #<?php echo $model->nama_toko; ?></h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('toko/index'); ?>" class="btn btn-info" title="List Toko"><i class="fa fa-bars"></i></a>
			<a href="<?php echo Yii::app()->createUrl('toko/create'); ?>" class="btn btn-primary" title="Tambah Toko"><i class="fa fa-plus"></i></a>
			<a href="<?php echo Yii::app()->createUrl('toko/update/'.$model->toko_id); ?>" class="btn btn-warning" title="Update Toko"><i class="fa fa-pencil"></i></a>
			<a href="<?php echo Yii::app()->createUrl('toko/delete/'.$model->toko_id); ?>" class="btn btn-danger" title="Delete Toko"><i class="fa fa-trash"></i></a>
			<a href="<?php echo Yii::app()->createUrl('toko/admin'); ?>" class="btn btn-success" title="Kelola Toko"><i class="fa fa-cogs"></i></a>
		</span>
	</div>
	<div style="margin: 10px;">
		<?php $this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
				'toko_id',
				'nama_toko',
				'alamat',
				'telepon',
				'kota',
				'tipe',
			),
		)); ?>
	</div>	
	<br>
</div>