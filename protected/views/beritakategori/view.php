<?php
/* @var $this BeritakategoriController */
/* @var $model KategoriBerita */

$this->breadcrumbs=array(
	'Kategori Beritas'=>array('index'),
	$model->berita_kategori_id,
);

$this->menu=array(
	array('label'=>'List KategoriBerita', 'url'=>array('index')),
	array('label'=>'Create KategoriBerita', 'url'=>array('create')),
	array('label'=>'Update KategoriBerita', 'url'=>array('update', 'id'=>$model->berita_kategori_id)),
	array('label'=>'Delete KategoriBerita', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->berita_kategori_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage KategoriBerita', 'url'=>array('admin')),
);
?>

<h1>View KategoriBerita #<?php echo $model->berita_kategori_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'berita_kategori_id',
		'nama_kategori',
		'slug',
	),
)); ?>
