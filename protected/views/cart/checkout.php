<!-- Featured -->
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/plugin/select2/css/select2.min.css">
<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/plugin/select2/js/select2.full.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/html/plugin/select2/js/i18n/id.js"></script>

<div class="col-lg-9 col-md-9 col-sm-12">
	<div class="col-lg-12 col-sm-12 hidden-print">
		<span class="title">Checkout Belanja</span>
	</div>
	
<form action="<?php echo Yii::app()->request->baseUrl; ?>/cart/success" class="form-horizontal" method="post" accept-charset="utf-8">
<input type="hidden" name="pelangganid" value="1"/>
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label class="col-md-3 control-label" for="">Kepada</label>
			<div class="col-md-9">
				<p class="form-control-static"><?php echo Yii::app()->user->customerName; ?></p>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="">Alamat</label>
			<div class="col-md-9">
				<p class="form-control-static"><?php echo $value['address'] ?></p>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="col-md-3 control-label" for="">Handphone</label>
			<div class="col-md-9">
				<p class="form-control-static"><?php echo $value['phone_number']; ?></p>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="">Kota</label>
			<div class="col-md-9">
				<p class="form-control-static"><?php echo $value['city']; ?></p>
			</div>
		</div>		
	</div>	
	<div class="col-md-4">
		<b>Total Bayar : </b>
		<h3 id="totalbayar"></h3>		
		<button type="submit" id="oksimpan" class="btn btn-success btn-flat" style="display: none">Pembayaran</button>
	</div>
</div>
<hr style="border-bottom: 1px dotted #ccc"/>
<div class="">

		<table class="table table-bordered">
		<thead>
			<th>Nama Produk</th>
			<th width="10%">Jumlah</th>
			<th width="20%">Harga</th>
			<th width="20%">Diskon</th>
			<th width="20%">Sub Total</th>
		</thead>
		<tbody>
			<tr>
				<td>
					A0001-Sepatu Ibu-ibu<br/>
					Berat : 30 gram<br/>
					Keterangan : warna merah					
				</td>
				<td>2</td>
				<td>Rp 169,000</td>
				<td>Rp 30,000</td>
				<td>Rp 278,000</td>			
				</tr>
				<input type="hidden" name="produk[1][produkid]" value="7"/>
				<input type="hidden" name="produk[1][qty]" value="2"/>
				<input type="hidden" name="produk[1][harga]" value="139000"/>
				<input type="hidden" name="produk[1][keterangan]" value="warna merah"/>
				<input type="hidden" name="produk[1][subtotal]" value="278000"/>
		</tbody>
			<tfoot>
				<tr>
					<td colspan="4">Total</td>
					<td>Rp 278,000</td>
				</tr>
			</tfoot>
		</table>

</div>
<div class="form-group">
	<label class="col-sm-2 control-label" for="">Pilihan Kurir</label>
	<div class="col-md-10">			
		<label class="radio-inline">
		<input type="radio" name="kurir" class="kurir" value="jne"/> JNE				</label>
				
		<label class="radio-inline">
		<input type="radio" name="kurir" class="kurir" value="pos"/> POS				</label>
				
		<label class="radio-inline">
		<input type="radio" name="kurir" class="kurir" value="tiki"/> TIKI				</label>
	</div>
</div>
<div id="kuririnfo" style="display: none;">
	<div class="form-group">
		<label class="col-sm-2 control-label" for="">Service</label>
		<div class="col-md-10">
			<p class="form-control-static" id="kurirserviceinfo"></p>
		</div>
	</div>
</div>

<input type="hidden" name="total" id="total" value="278000"/>
<input type="hidden" name="ongkir" id="ongkir" value="0"/>
<input type="hidden" name="berat" value="30"/>
<input type="hidden" name="diskonnilai" id="diskonnilai" value="30000"/>
</form>

<script>
$(document).ready(function(){

$(".kurir").each(function(o_index,o_val){
	$(this).on("change",function(){
		var did=$(this).val();
		var berat="30";
		var kota="48";
		$.ajax({
		  method: "get",
		  dataType:"html",
		  url: "http://localhost/neraca/produk/kurirdata",
		  data: "kurir="+did+"&berat="+berat+"&kota="+kota,
		  beforeSend:function(){
		  	$("#oksimpan").hide();
		  }
		})
		.done(function( x ) {			
		    $("#kurirserviceinfo").html(x);
		    $("#kuririnfo").show();		    
		})
		.fail(function(  ) {
			$("#kurirserviceinfo").html("");
		    $("#kuririnfo").hide();
		});
	});
});

$("#diskon").html(toDuit(30000));
hitung();

});

function hitung()
{
	var diskon=$('#diskonnilai').val();
	var total=$('#total').val();
	var ongkir=$("#ongkir").val();
	var bayar=(parseFloat(total)+parseFloat(ongkir));
	if(parseFloat(ongkir) > 0)
	{
		$("#oksimpan").show();
	}else{
		$("#oksimpan").hide();
	}
	$("#totalbayar").html(toDuit(bayar));
}

</script>
		</div>
    	<!-- End Featured -->

    	<div class="clearfix visible-sm"></div>
    </div>
</div>
