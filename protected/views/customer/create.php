<?php

?>

<h3>Tambah Akun Customer</h3>
<hr>
<div class='form-group'>
    <?php
        echo CHtml::beginForm(array('customer/create'));
    ?>
     
    <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'customer_name', array('class'=>'label-control'));
            echo CHtml::activeTelField($model, 'customer_name', array('class'=>'form-control'));
        ?>
    </div>
    <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'email', array('class'=>'label-control'));
            echo CHtml::activeTelField($model, 'email', array('class'=>'form-control'));
        ?>
    </div>
    <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'password', array('class'=>'label-control'));
            echo CHtml::activePasswordField($model, 'password', array('class'=>'form-control'));
        ?>
    </div>
    <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'comparePassword', array('class'=>'label-control'));
            echo CHtml::activePasswordField($model, 'comparePassword', array('class'=>'form-control'));
        ?>
    </div>
    <div class='form-group pull-right'>
        <?php
            echo CHtml::submitButton('Add', array('class'=>'btn btn-primary btn-flat'));
            echo CHtml::endForm();
        ?>
    </div>
</div>