<?php
/* @var $this customersController */
/* @var $model customers */

$this->breadcrumbs=array(
	'Customer'=>array('index'),
	$model->customer_id,
);

$this->menu=array(
	array('label'=>'List customer', 'url'=>array('index')),
	array('label'=>'Create customer', 'url'=>array('create')),
	array('label'=>'Update customer', 'url'=>array('update', 'customer_id'=>$model->customer_id)),
	array('label'=>'Delete customer', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','customer_id'=>$model->customer_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage customers', 'url'=>array('admin')),
);
?>
<br>  
<div class="box box-primary">
	<div class="box-header with-border">
      	<h3 class="box-title">View customers #<?php echo $model->customer_id; ?></h3>
		<span style="float: right;padding-top: 15px;">
			<a href="<?php echo Yii::app()->createUrl('customer/index'); ?>" class="btn btn-info" title="List customers Transfer"><i class="fa fa-bars"></i></a>
			<a href="<?php echo Yii::app()->createUrl('customer/create'); ?>" class="btn btn-primary" title="Tambah customers Transfer"><i class="fa fa-plus"></i></a>
			<a href="<?php echo Yii::app()->createUrl('customer/update/'.$model->customer_id); ?>" class="btn btn-warning" title="Update customers Transfer"><i class="fa fa-pencil"></i></a>
			<a href="<?php echo Yii::app()->createUrl('customer/delete/'.$model->customer_id); ?>" class="btn btn-danger" title="Delete customers Transfer"><i class="fa fa-trash"></i></a>
			<a href="<?php echo Yii::app()->createUrl('customer/admin'); ?>" class="btn btn-success" title="Kelola customers Transfer"><i class="fa fa-cogs"></i></a>
		</span>
	</div>
	<div style="margin: 10px;">
		<?php $this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
				'customer_id',
				'customer_name',
				'email',
				'password',
				// 'customer_id_admin',
			),
		)); ?>		
	</div>	
	<br>
</div>	