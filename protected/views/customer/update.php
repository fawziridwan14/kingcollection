<?php 

?>
<h3>Update Data Member #<?php echo $model->customer_name; ?></h3>
<hr>
<div class='form-group'>
    <?php
        echo CHtml::beginForm(array('customer/update/'.$model->customer_id));      
    ?>
     
    <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'customer_name', array('class'=>'label-control'));
            echo CHtml::activeTelField($model, 'customer_name', array('class'=>'form-control'));
        ?>
    </div>
    <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'email', array('class'=>'label-control'));
            echo CHtml::activeTextArea($model, 'email', array('class'=>'form-control'));
        ?>
    </div>
    <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'password', array('class'=>'label-control'));
            echo CHtml::activePasswordField($model, 'password', array('class'=>'form-control'));
        ?>
    </div>
    <div class='form-group'>
        <?php
            echo CHtml::activeLabel($model, 'comparePassword', array('class'=>'label-control'));
            echo CHtml::activePasswordField($model, 'comparePassword', array('class'=>'form-control'));
        ?>
    </div>
    <div class='form-group pull-right'>
        <?php
            echo CHtml::submitButton('Update', array('class'=>'btn btn-primary btn-flat'));
            echo CHtml::endForm();
        ?>
    </div>
</div>