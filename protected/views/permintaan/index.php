<?php
/* @var $this PermintaanController */
/* @var $dataProvider CActiveDataProvider 

$this->breadcrumbs=array(
	'Permintaans',
);

$this->menu=array(
	array('label'=>'Create Permintaan', 'url'=>array('create')),
	array('label'=>'Manage Permintaan', 'url'=>array('admin')),
);*/
?>

<h3 class="box-title">Permintaan Produk</h3>
<br>
<div class="box box-primary">
	<div class="box-header">
		<span style="float: right;padding-top: 15px; margin: 5px;">
			<a href="<?php echo Yii::app()->createUrl('permintaan/create'); ?>" class="btn btn-info btn-flat btn-sm" title="List Permintaaan"><i class="fa fa-plus"></i></a>
			<a href="<?php echo Yii::app()->createUrl('permintaan/admin'); ?>" class="btn btn-danger btn-flat btn-sm" title="Kelola Permintaaan"><i class="fa fa-cogs"></i></a>
		</span>
	</div>
	<div style="margin: 10px;">
		<table id="example2" class="table table-bordered">
			<thead>
				<tr style="width: 100%;">
					<th>ID</th>
					<th>Tanggal</th>
					<th>Supplier</th>
					<th>Produk</th>
					<th>Jumlah</th>	
					<th><center>Aksi</center></th>			
				</tr>
			</thead>
			<tbody>
			<?php 
			/*$this->widget('zii.widgets.CListView', array(
				'dataProvider'=>$dataProvider,
				'itemView'=>'_view',
			)); foreach($model as $data):*/ 
			for ($i=0; $i < sizeof($cursor); $i++) { 
				$model = $cursor[$i];
			?>
				<tr style="width: 100%;">
					<td><?php echo $model->id; ?></td>
					<td><?php echo $model->tanggal; ?></td>
					<td><?php echo $model->supplier_relasi->nama_supplier; ?></td>
					<td><?php echo $model->product_relasi->product_name; ?></td>
					<td><?php echo $model->jumlah; ?></td>	
					<td><center>
						<a href="<?php echo $model['id']; ?>" class="btn btn-info btn-flat btn-xs"><i class="fa fa-info fa-sm"></i> Detail</a>
						<a href="<?php echo Yii::app()->request->baseUrl;?>/permintaan/report" class="btn btn-default btn-flat btn-xs"><i class="fa fa-print fa-sm"></i> export</a>
						</center>
					</td>				
				</tr>
			<?php } /*endforeach;*/ ?>
			</tbody>
		</table>
	</div>
	<br>
</div>