<div class="col-lg-9 col-md-9 col-sm-12">
	<br>
	<?php $this->renderPartial('_myaccount_menu');?>
	<div class="col-lg-12 col-sm-12 hidden-print" style="text-align: justify;">
		<span class="title"><br>Ubah Password</span> 
	</div>
	<div class="col-lg-9 col-md-9 col-sm-12 hidden-print" style="text-align: justify;">
		
		<div  class="form-horizontal" style="margin-left: 15px; clear: both;" accept-charset="utf-8">
		        
		        <?php $form = $this -> beginWidget('CActiveForm', 
		        array('id' => 'customer-form', 
		        	'enableAjaxValidation' => false,
		        	'enableClientValidation'=>TRUE, 
				)); ?>

		        <p class="note">
		            Fields with <span class="required">*</span> are required.
		        </p>

		        <?php echo $form -> errorSummary($model); ?>

		        <div class="form-group">
		            <div class="col-md-8">
            <?php echo $form -> labelEx($model, 'oldPassword', array('class'=>'control-label')); ?>
            <?php echo $form -> hiddenField($model, 'password', array('size' => 35, 'maxlength' => 35)); ?>
            <?php echo $form -> passwordField($model, 'oldPassword', array('size' => 35, 'maxlength' => 35, 'class'=>'form-control','placeholder'=>'Current Password')); ?>
            <?php echo $form -> error($model, 'oldPassword'); ?>
		            </div>
		        </div>

		        <div class="form-group">
		            <div class="col-md-8">
            <?php echo $form -> labelEx($model, 'newPassword', array('class'=>'control-label')); ?>
            <?php echo $form -> passwordField($model, 'newPassword', array('size' => 35, 'maxlength' => 35, 'class'=>'form-control','placeholder'=>'New Password')); ?>
            <?php echo $form -> error($model, 'newPassword'); ?>
		            </div>
		        </div>

		        <div class="form-group">
		            <div class="col-md-8">
            <?php echo $form -> labelEx($model, 'compareNewPassword', array('class'=>'control-label')); ?>
            <?php echo $form -> passwordField($model, 'compareNewPassword', array('size' => 35, 'maxlength' => 35, 'class'=>'form-control','placeholder'=>'Confirm Password')); ?>
            <?php echo $form -> error($model, 'compareNewPassword'); ?>
		            </div>
		        </div>

		        <div class="form-group">
		            <label class="col-sm-5 control-label">&nbsp;</label>
		            <div class="col-md-5">
		                <?php echo CHtml::submitButton('Ubah Password', array('class'=>'btn btn-primary btn-flat btn-md')); ?>
		            </div>
		        </div> 	
	    </div>		
        <?php $this -> endWidget(); ?>
	</div>
</div>