 <div class="col-lg-9 col-md-9 col-sm-12">
	<div class="col-lg-12 col-sm-12 hidden-print" style="text-align: justify;">
		<span class="title">Buku Alamat</span>
	</div>
	<?php $this->renderPartial('_myaccount_menu');?>
	<?php 
		foreach (Yii::app()->user->getFlashes() as $key => $message) {
			echo "<div class='alert alert-success flash-".$key."'>".$message."</div>";
		}
	?> 
	<div class="col-lg-12 col-sm-12 hidden-print" style="text-align: justify;">
	<br>
		<?php $this->renderPartial('_formAddAddress', array('model'=>$model));?>
	</div>
</div>