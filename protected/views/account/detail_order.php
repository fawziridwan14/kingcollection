 <div class="col-lg-9 col-md-9 col-sm-12">
	<div class="col-lg-12 col-sm-12 hidden-print" style="text-align: justify;">
		<span class="title">Detail Order</span>
	</div>

	<div>
		<?php $this -> renderPartial('_myaccount_menu'); ?>	
	</div>

	<div>
	<br>
	<strong style="margin-left:15px;float:left;padding: 0 0 15px 0;">
		Hallo 
		<font color="green" style="text-transform: uppercase;"><?php echo $dataOrder['customer_name']; ?>,</font>
		<br>berikut data order anda dengan 
		Nomor Pemesanan <font color="green"><?php echo $dataOrder['order_code']; ?></font></strong> 
	<div style="clear: left;"></div> 
		<div style="margin: 15px;">
			<table class="table table-responsive table-bordered">
				<tr>
					<td>Nama</td>
					<!-- <td>:</td> -->
					<td><?php echo $dataOrder['customer_name']; ?></td>
				</tr>
				<tr>
					<td>Nomor Pemesanan</td>
					<!-- <td>:</td> -->
					<td><?php echo $dataOrder['order_code']; ?></td>
				</tr>
				
				<tr>
					<td>Tanggal Order</td>
					<!-- <td>:</td> -->
					<td><?php echo date('d F Y', strtotime($dataOrder['order_date'])); ?></td>
				</tr>
				<tr>
					<td>Bank Transfer</td>
					<!-- <td>:</td> -->
					<td><?php echo $dataOrder['bank_transfer']; ?></td>
				</tr>
				<tr>
					<td>Status Pembayaran</td>
					<!-- <td>:</td> -->
					<td>
					<?php
					if($dataOrder['payment_status']==0){
					?>
					<!--link untuk konfirmasi pembayaran-->

					<a class="label label-primary label-flat" title="click to confirm payment" href="<?php echo $this->createUrl('account/orders',array('confirm'=>$dataOrder['order_code']));?>" style="text-decoration: none; margin-right: 20px;"><span class="label label-primary label-flat" style="margin-right: 15px;"></span> Konfirmasi Pembayaran</a>
					<?php }else{ ?> 
					<span class="label label-success label-flat" style="margin-right: 15px;">Pembayaran telah dikonfirmasi</span>
					<?php } ?>					
					</td>
				</tr>
				<tr>
					<td>Ongkos Kirim</td>
					<td>IDR <?php echo number_format($dataOrder['ongkir'],2,",",".") ?></td>
				</tr>
			</table>	
			<!--/data pemesan-->

			<div style="clear: both;">
				&nbsp;
			</div>
 
			<!--data detail order-->
			<div class="grid-view">
				<table class="table table-responsive table-bordered">
					<thead>
						<tr>
							<th>No.</th>
							<th>Nama Produk</th>
							<th>Harga</th>
							<th>Jumlah</th>
							<th>Sub Total</th>
						</tr>
					</thead>
					<tbody>
					<?php 
					foreach($orderDetail as $key=>$detail):
					$class = ($key+1)%(2) ==0 ? 'even' : 'odd';
					$subtotal = $detail['price']*$detail['qty'];
					$grandtotal +=$subtotal+$detail['ongkir']; 
					?> 
						<tr class="<?php echo $class; ?>">
							<td><?php echo $key + 1; ?></td>
							<td><?php echo $detail['product_name']; ?></td>
							<td align="right">IDR <?php echo number_format($detail['price'],2,",","."); ?></td> 
							<td align="center"><?php echo $detail['qty']; ?></td>
							<td align="right">IDR <?php echo number_format($subtotal,2,",","."); ?></td>
						</tr> 
						<?php endforeach; ?>
						<tr>
							<td colspan="4" align="right">Ongkos Kirim :</td>
							<td align="right">IDR <?php echo number_format($detail['ongkir'],2,",","."); ?></td>
						</tr> 
						<tr>
							<td colspan="4" align="right">Grand Total :</td>
							<td align="right">IDR <?php echo number_format($grandtotal,2,",","."); ?></td>
						</tr> 
					</tbody>
				</table>
			</div>
			<!--/data detail order-->				
		</div>
	</div>
</div>