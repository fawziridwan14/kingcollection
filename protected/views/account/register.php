<div class="col-lg-9 col-md-9 col-sm-12">
                <div class="col-lg-12 col-sm-12 hidden-print">
                    <span class="title">Member Register</span>
                </div>

    <div  class="form-horizontal" style="margin-left: 15px; clear: both;" accept-charset="utf-8">
        
        <?php $form = $this -> beginWidget('CActiveForm', 
        array('id' => 'customer-form', 
            'enableAjaxValidation' => false,
            'enableClientValidation'=>TRUE, 
        )); ?>

        <p class="note">
            Fields with <span class="required">*</span> are required.
        </p>

        <?php echo $form -> errorSummary($model); ?>

        <div class="form-group">    
            <div class="col-md-8">
                <?php echo $form -> labelEx($model, 'Nama Lengkap', array('class'=>'control-label')); ?>
                <?php echo $form -> textField($model, 'customer_name', array('class'=>'form-control','placeholder'=>'Nama Customer','size' => 57, 'maxlength' => 57)); ?>
                <?php echo $form -> error($model, 'customer_name'); ?>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-md-8">
                <?php echo $form -> labelEx($model, 'email', array('class'=>'control-label')); ?>
                <?php echo $form -> textField($model, 'email', array('class'=>'form-control','placeholder'=>'Email','size' => 45, 'maxlength' => 45)); ?>
                <?php echo $form -> error($model, 'email'); ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-8">
                <?php echo $form -> labelEx($model, 'password',array('class'=>'control-label')); ?>
                <?php echo $form -> passwordField($model, 'password', array('class'=>'form-control','placeholder'=>'Password','size' => 35, 'maxlength' => 35)); ?>
                <?php echo $form -> error($model, 'password'); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-8">
                <?php echo $form -> labelEx($model, 'comparePassword',array('class'=>'control-label')); ?>
                <?php echo $form -> passwordField($model, 'comparePassword', array('class'=>'form-control','placeholder'=>'Confirm Password','size' => 35, 'maxlength' => 35)); ?>
                <?php echo $form -> error($model, 'comparePassword'); ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-8">
                <?php $this->widget('CCaptcha'); ?>
                <?php echo $form->textField($model,'captcha', array('class'=>'form-control','placeholder'=>'Put Verify Code')); ?> 
                <?php echo $form->error($model,'captcha'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-8 control-label">&nbsp;</label>
            <div class="col-md-5">
                <?php echo CHtml::submitButton('Register', array('class'=>'btn btn-primary btn-flat btn-md')); ?>
            </div>
            <?php $this -> endWidget(); ?>
        </div> 

    </div>

</div>

</div>