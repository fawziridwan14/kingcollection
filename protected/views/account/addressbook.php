 <div class="col-lg-9 col-md-9 col-sm-12">
	<div class="col-lg-12 col-sm-12 hidden-print" style="text-align: justify;">
		<span class="title">Buku Alamat</span>
	</div>
	
	<div class="menu">
		<?php $this->renderPartial('_myaccount_menu');?>
	</div>
	<div class="message">
	<?php 
		foreach (Yii::app()->user->getFlashes() as $key => $message) {
			echo "<div class='alert alert-success flash-".$key."'>".$message."</div>";
		}
	?> 
	</div>
		
	<?php 
		$i=1;

		foreach ($model as $address):
			($i%3 ===0) ? 
			$div = '<div style="clear: top;"></div>':$div='';
	?>
	<br>

	<div class="col-lg-12 col-sm-12 hidden-print" style="text-align: justify;">

		<div class="table table-responsive">
			<table class="table table-bordered">
				<tr>
					<th valign="top">Nama</th>
					<td valign="top"><?php echo $address['name']; ?></td>
				</tr>
				<tr>
					<th valign="top">Telp</th>
					<td valign="top"><?php echo $address['phone_number']; ?></td>
				</tr>
				<tr>
					<th valign="top">Alamat</th>
					<td valign="top"><?php echo $address['address']; ?></td>
				</tr>
				<tr>
					<th valign="top">Provinsi</th>
					<td valign="top"><?php echo $address['province']; ?></td>
				</tr>
				<tr>
					<th valign="top">Kota</th>
					<td valign="top"><?php echo $address['city']; ?></td>
				</tr>
				<tr>
					<td colspan="3" align="right">
						<a href="<?php echo $this->createUrl('account/addressbook',array('add'=>'addressbook'));?>"> <i class="btn btn-primary btn-flat fa fa-plus"> Tambahkan Alamat Baru</i></a>
						<a href="<?php echo $this->createUrl('account/addressbook',array('edit'=>$address->id_address));?>"><i class="btn btn-warning btn-flat fa fa-pencil"> Edit Alamat</i></a>
					</td>
				</tr>

			</table>
		</div>
	</div>

	
	<?php
		echo $div;
		$i++; 
		endforeach; 
	?>
	<div style="clear: both;">&nbsp;</div>
</div>