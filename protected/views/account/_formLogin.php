<!-- Featured -->
<div  class="form-horizontal" style="margin-left: 15px; clear: both;" accept-charset="utf-8">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	)); ?>

	<div class="form-group">
		<div class="col-md-9">		
			<?php echo $form->labelEx($model,'email', array('class'=>'control-label')); ?>
			<?php echo $form->TextField($model,'email', array('class'=>'form-control','placeholder'=>'Email')); ?>
			<?php echo $form->error($model,'email', array('class'=>"alert alert-danger")); ?>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-9">
			<?php echo $form->labelEx($model,'password', array('class'=>'control-label')); ?>
			<?php echo $form->passwordField($model,'password', array('class'=>'form-control input-search','placeholder'=>'Password')); ?>
			<?php echo $form->error($model,'password', array('class'=>"alert alert-danger")); ?>
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-9">
			<?php echo $form->checkBox($model,'rememberMe', array('class'=>'control-label label-primary')); ?>
			<?php echo $form->label($model,'rememberMe'); ?>
			<?php echo $form->error($model,'rememberMe'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-6 control-label">&nbsp;</label>
			<div class="col-md-5">
				<?php echo CHtml::submitButton('Login', array('class'=>'btn btn-primary btn-md')); ?>
			</div>
	</div>
</div>
<?php $this->endWidget(); ?>